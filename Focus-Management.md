# **Focus Management**

# I. What is Deep Work?

## **1. What is Deep Work?**

A person is said to be in deep work when he is working on something that is cognitively demanding, with full focus without being distracted by anything.

# II. The optimal duration for deep work

Video: 3:20 minutes - https://www.youtube.com/watch?v=LA6mvxwecZ0

# III. Are deadlines good for productivity?

Video: 4:52 seconds - https://www.youtube.com/watch?v=Jkl1vMNvvHU

# IV. Summary of Deep Work Book

Video: 7:30 minutes- https://www.youtube.com/watch?v=gTaJhjQHcf8

## **2. Paraphrase all the ideas in the above videos and this one in detail.**

1. The optimal duration for deep work is around 1 to 1.5 hours. During 1 Hour of coding, only 45 minutes go into actual coding while the 15 minutes go for thinking time and looking at references. Also, one should not be distracted during this deep work time and should stay away from all kinds of distractions that are in completely different contexts.

2. Deadlines are good for productivity, as a person innately wants to complete the work before the deadline and does not slack off and take unnecessarily long breaks.

3. Intense periods of focus in isolated fields of work cause myelin to develop in relevant areas of the brain which allows brain cells to fire faster and clearer. In other words, deep work upgrades our brains and can help us rapidly connect ideas and uncover creative solutions. Deep work is becoming increasingly rare and valuable. We can resist temptations and take breaks only with a 50% success rate. The following 3 steps help us to incorporate a deep work strategy in our day-to-day life:

- Schedule Distraction Periods at home and work
- Develop a rhythmic deep work ritual
- Have a daily shutdown complete ritual

## **3. How can you implement the principles in your day-to-day life?**

- I should schedule my time for deep work and distractions and keep them apart completely
- I should do any work with a set deadline in mind, to minimize slacking off on work
- I should develop a deep work ritual every day to atleast achieve 4 hours of deep work

# V. Dangers of Social Media

Video: 13:50 minutes - https://www.youtube.com/watch?v=3E7hkPZ-HTk

## **4. Your key takeaways from the video**

- Social media is not a fundamental technology and is simply an entertainment product.
- Use of social media is not at the core of anyone's professional success.
- Social media tools are designed to be addictive as it aims to fragment one's attention as much as possible during the waking hours
- Spending a significant amount of time on social media permanently reduce the capacity for concentration.
- Life without social media has real positives associated with it.
