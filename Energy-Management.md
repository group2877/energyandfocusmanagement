# **Energy Management**

# I. Manage Energy Not Time

## **1. What are the activities you do that make you relax - Calm quadrant?**

Some of the activities that I do to relax are listed as follows:

- Listening to Music
- Outdoor Walking
- Power Naps
- Watching a movie
- Playing Video Games
- Hanging out with friends

## **2. When do you find getting into the Stress quadrant?**

I find myself in the stress quadrant in the following situations:

- Finding constant failure when doing a work
- Being close to a deadline of a work that's pending
- Being not able to complete work on time

## **3. How do you understand if you are in the Excitement quadrant?**

I understand that I'm in the Excitement Quadrant when I'm interacting with people that give me joy or when I'm thoroughly enjoying a movie, a song, or a game.

# II. Sleep is your Superpower

## **4. Paraphrase the Sleep is your Superpower video in detail.**

The video essentially says that a lack of sleep will age a person by a decade in terms of his/her wellness. It also talks about a study where people were categorized into two groups: the sleep group and the sleep deprivation group. The sleep group was allowed to sleep 8 hours a day while the deprivation group was kept awake in a laboratory under full supervision. When their learning ability was measured, it was found that there was a 40-percent deficit in the ability of the brain to make new memories without sleep. The hippocampus in the brain acts as an informational inbox to the brain, and its activity is disrupted if a person is deprived of sleep.

Sleep and diseases like dementia are interrelated. Disruption of deep sleep is an important factor that is contributing to cognitive decline or memory decline in aging, and in Alzheimer's disease as well. In the spring, when we lose one hour of sleep, we see a 24-percent increase in heart attacks that following day. In the autumn, when we gain an hour of sleep, we see a 21-percent reduction in heart attacks. The same pattern can be seen for car crashes, road traffic accidents, and even suicide rates. Natural killer cells that are very good at identifying and eliminating dangerous, unwanted elements are greatly affected if you're not sleeping enough. Sleeping for 4 hours can cause a 70% reduction in natural killer cell activity.

From these facts, it is very evident that the shorter you sleep, the shorter your life will be. To get a good night's sleep, one should decrease consuming alcohol and caffeine, avoid naps during the day, regularly sleep and wake up at the same time everyday. Also, our body needs to drop its core temperature by about two to three degrees Fahrenheit to initiate sleep, so keep the room at a temperature of about 65 degrees Fahrenheit or 18 degrees Celsius. Sleep is a non-negotiable biological necessity that shouldn't be tampered with.

## **5. What are some ideas that you can implement to sleep better?**

- I should reduce consuming caffeine
- I should sleep in a room that is around 18 degrees celsius to cool my body quickly
- I should keep a regular time to fall asleep and wake up
- I should sleep for at least 8 hours a day for better health

# III. Brain-changing Benefits of Exercise

## **6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.**

The video essentially talks about the powerful effects of physical activity through the following points:

- Regular exercise provides a great mood and energy boost.
- A single workout immediately increases secretion of neurotransmitters like dopamine, serotonin, and noradrenaline, which can improve your ability to shift and focus attention,which will last for at least two hours and can improve your reaction times.
- Exercise changes the brain's anatomy, physiology, and function. It produces new brain cells in the hippocampus that significantly improve your long-term memory.
- The rule of thumb is to exercise 3 to 4 times a week with a minimum 30 minutes exercise session to see long-lasting effects.
- One should go beyond the rule of thumb and exercise according to the optimum exercise prescription dependent on one's age, fitness level, and genetic background.

## **7. What are some steps you can take to exercise more?**

- I should find some time left in the day for exercise
- I should exercise along with a proper diet to attain its benefits.
- I should not stop exercising once I start it and should be regular in doing it.
